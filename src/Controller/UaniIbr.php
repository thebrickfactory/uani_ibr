<?php

namespace Drupal\uani_ibr\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\facets\Exception\Exception;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;
use Drupal\uani_ibr\xlsWorker\graphXls;
use Drupal\uani_ibr\xlsWorker\lightboxXls;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

class UaniIbr extends ControllerBase {

  public function __construct() {
    $this->tempStore = new Session();
  }

  public function renderIndex() {
    $data = array();

    $graph_upload = \Drupal::formBuilder()->getForm('Drupal\uani_ibr\Form\GraphDataForm');
    $templates['graph']['upload'] = render($graph_upload);
    $lightbox_upload = \Drupal::formBuilder()->getForm('Drupal\uani_ibr\Form\LightboxDataForm');
    $templates['lightbox']['upload'] = render($lightbox_upload);
    $images_upload = \Drupal::formBuilder()->getForm('Drupal\uani_ibr\Form\ImagesDataForm');
    $templates['images']['upload'] = render($images_upload);

    $uploads_raw = \Drupal::database()->select('uani_ibr_uploads', 'ibru');
    $uploads_raw->fields('ibru');
    $uploads_raw = $uploads_raw->execute()->fetchAll();
    $last_graph_import = \Drupal::state()->get('last_graph_import');
    $last_lightbox_import = \Drupal::state()->get('last_lightbox_import');
    if (!empty($uploads_raw)) {
      foreach ($uploads_raw as $value) {
        $file = File::load($value->file_id);
        $data[$value->type]['list'][] = [
          'id' => $value->id,
          'type' => $value->type,
          'file' => $file,
          'usage'=> \Drupal::service('file.usage')->listUsage($file),
          'last_graph_import' => $last_graph_import,
          'last_lightbox_import' => $last_lightbox_import,
          'user' => User::load($value->user_id)
        ];
      }
    }

    return [
      '#title' => t('IBR settings'),
      '#theme' => 'uani_ibr_admin_settings',
      '#data' => $data,
      '#templates' => $templates,
    ];
  }

  public function renderImport(Request $request) {
    $id = ltrim($request->getRequestUri(), '/admin/config/uani/ibr/import/');
    $upload = \Drupal::database()
                     ->select('uani_ibr_uploads', 'ibru')
                     ->condition('ibru.id', $id)
                     ->fields('ibru')
                     ->execute()
                     ->fetchObject();
    if (!empty($upload)) {
      if ($upload->type == 'images') {
        drupal_set_message('Error, you can\'t process image!', 'error');
      }
      else {
        $file = File::load($upload->file_id);
        if ($upload->type == 'graph') {
          $xls = new graphXls($file);
        }
        if ($upload->type == 'lightbox') {
          $xls = new lightboxXls($file);
        }
        if (!empty($xls)) {
          $xls->validateXls();
          if ($xls->xlsStructOk) {
            if ($upload->type == 'graph') {
              if ($xls->import()) {
                drupal_set_message($this->t('File imported!'), 'status');
              }
              else {
                drupal_set_message($this->t('Failed to import file!'), 'error');
              }
            }
            if ($upload->type == 'lightbox') {
              drupal_set_message($this->t('Lightbox file validated! Now you must import graph file.'), 'status');
            }
          }
          else {
            drupal_set_message($this->t('File structure invalid! Please fix issues listed above and try again!'), 'error');
          }
        }
      }
    }
    return $this->afterSubmitRedirect();
  }

  public function renderRemove(Request $request) {
    $id = ltrim($request->getRequestUri(), '/admin/config/uani/ibr/remove/');
    $upload = \Drupal::database()
                     ->select('uani_ibr_uploads', 'ibru')
                     ->condition('ibru.id', $id)
                     ->fields('ibru')
                     ->execute()
                     ->fetchObject();
    if (\Drupal::database()
               ->delete('uani_ibr_uploads')
               ->condition('id', $upload->id)
               ->execute()) {
      File::load($upload->file_id)->delete();
      drupal_set_message($this->t('File deleted!'), 'status');
    }
    else {
      drupal_set_message($this->t('Failed to delete file!'));
    }
    return $this->afterSubmitRedirect();
  }

  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function afterSubmitRedirect() {
    return $this->redirect('uani_ibr.admin.index');
  }

}