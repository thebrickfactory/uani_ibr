<?php

namespace Drupal\uani_ibr\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\uani_ibr\Controller\UaniIbr;

class LightboxDataForm extends FormBase {

  public function getFormId() {
    return 'uani_ibr_lightbox_data_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['lightbox_data_file'] = array(
      '#type' => 'managed_file',
      '#title' => $this->t('Choose a file with lightbox data'),
      '#upload_location' => 'public://ibr/lightbox/',
      '#upload_validators' => [
        'file_validate_extensions' => ['xls csv'],
      ],
      '#attached' => [
        'library' => ['file/drupal.file'],
      ],
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save')
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file = File::load($form_state->getValue('lightbox_data_file')[0]);
    $file->setFilename(mb_strtolower(str_replace(' ', '_', $file->getFilename())));
    $file->setPermanent();
    $file->save();
    \Drupal::database()->insert('uani_ibr_uploads')
           ->fields(['type', 'file_id', 'user_id'], ['lightbox', $file->id(), \Drupal::currentUser()->id()])
           ->execute();
    drupal_set_message($this->t('File uploaded and saved!'), 'status');
    $controller = new UaniIbr();
    $controller->afterSubmitRedirect();
  }

}