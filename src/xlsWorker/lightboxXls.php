<?php

namespace Drupal\uani_ibr\xlsWorker;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\facets\Exception\Exception;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

class lightboxXls extends xls {

  public function validateXls() {
    \Drupal::state()->set('last_lightbox_import', $this->file->id());
    if ($lightbox_data = $this->getDataArray()) {
      $valid = true;
      foreach ($lightbox_data as $value) {
        // validate title and body, check if string data
        if (!is_string($value['Title']['value'])) {
          $valid = false;
          $this->_validateCellBadValue($value['Title']['xls_cords']['row'], $value['Title']['xls_cords']['col'], 'Bad title');
        }
        if (!is_string($value['Body']['value'])) {
          $valid = false;
          $this->_validateCellBadValue($value['Body']['xls_cords']['row'], $value['Body']['xls_cords']['col'], 'Bad body');
        }
        // check metrics
        if (!empty($value['Metric'])) {
          foreach ($value['Metric'] as $metric) {
            if (!isset($metric['rank']['value'])) {
              $valid = false;
              $this->_validateCellBadValue($metric['rank']['xls_cords']['row'], $metric['rank']['xls_cords']['col'], 'Bad metric rank');
            }
            if (!isset($metric['source']['value']) || !is_string($metric['source']['value'])) {
              $valid = false;
              $this->_validateCellBadValue($metric['source']['xls_cords']['row'], $metric['source']['xls_cords']['col'], 'Bad metric source');
            }
            if (!isset($metric['description']['value']) || !is_string($metric['description']['value'])) {
              $valid = false;
              $this->_validateCellBadValue($metric['description']['xls_cords']['row'], $metric['description']['xls_cords']['col'], 'Bad metric description');
            }
            if (!isset($metric['citation']['value']) || !is_string($metric['citation']['value'])) {
              $valid = false;
              $this->_validateCellBadValue($metric['citation']['xls_cords']['row'], $metric['citation']['xls_cords']['col'], 'Bad metric citation');
            }
            if (isset($metric['image']['value'])) { // image field not required
              if (!is_string($metric['image']['value'])) {
                $valid = FALSE;
                $this->_validateCellBadValue($metric['image']['xls_cords']['row'], $metric['image']['xls_cords']['col'], 'Bad metric image');
              }
              else {
                $image_file_exits_query = \Drupal::entityQuery('file')
                                                 ->condition('filename', $metric['image']['value'])
                                                 ->condition('type', 'image')
                                                 ->count()->execute();
                if ($image_file_exits_query == 0) {
                  $valid = FALSE;
                  $this->_validateCellBadValue($metric['image']['xls_cords']['row'], $metric['image']['xls_cords']['col'], 'Specified image not found!');
                }
              }
            }
          }
        }
        else {
          $valid = false;
          drupal_set_message(new TranslatableMarkup('Metric not found!'), 'error');
        }
      }
      if ($valid) {
        $this->tempStore->set('lightbox_data_file_id', $this->file->id());
        $this->xlsStructOk = TRUE;
      }
    }
    $this->tempStore->set('lightbox_data_uploaded_and_validated', $this->xlsStructOk);
    return $this->xlsStructOk;
  }

  public function import() {
    if ($data = $this->getDataArray()) {
      $this->_deleteOldNodes();
      $inserted = array();
      foreach ($data as $item) {
        $new_node = Node::create([
          'type' => 'business_risk_lightbox_data',
          'title' => $item['Title']['value'],
          'body' => [
            'value' => $item['Body']['value'],
            'format' => 'full_html',
          ],
        ]);
        foreach ($item['Metric'] as $metric) {
          $paragraph = Paragraph::create([
            'type' => 'b_r_l_d_s',
            'field_b_r_l_d_s_company_text' => $metric['source']['value'],
            'field_b_r_l_d_s_iran_s_score' => $metric['rank']['value'],
            'field_b_r_l_d_s_score_desc' => $metric['description']['value'],
            'field_b_r_l_d_s_link' => $metric['citation']['value'],
          ]);
          if (isset($metric['image'])) {
            $metric_image_query = \Drupal::entityQuery('file')
                                         ->condition('filename', $metric['image']['value'])
                                         ->condition('type', 'image')
                                         ->execute();
            $paragraph->field_b_r_l_d_s_company_logo = [
              'target_id' => reset($metric_image_query),
            ];
          }
          $paragraph->save();
          $new_node->field_b_r_l_d_source[] = [
            'target_id' => $paragraph->id(),
            'target_revision_id' => $paragraph->getRevisionId()
          ];
        }
        $new_node->save();
        $inserted[] = [
          'item' => $item,
          'nid' => $new_node->id(),
        ];
      }
      return $inserted;
    }
    return false;
  }

  protected function _deleteOldNodes() {
    $nodes = \Drupal::entityQuery('node')
      ->condition('type', 'business_risk_lightbox_data')
      ->execute();
    if (!empty($nodes)) {
      foreach (Node::loadMultiple($nodes) as $node) {
        foreach ($node->get('field_b_r_l_d_source') as $p) {
          $paragraph = Paragraph::load($p->getValue()['target_id']);
          $paragraph->delete();
        }
        $node->delete();
      }
    }
  }

  public function getDataArray() {
    try {
      $sheet = $this->reader->getSheet(0);
    }
    catch(Exception $exception) {
      drupal_set_message($exception->getMessage());
      return false;
    }
    $raw_data = $sheet->toArray(null, true, true, true);

    $data = array();
    $heading = $raw_data[1];
    foreach ($raw_data as $row_key => $row_cols_value) {
      if ($row_key == 1) continue; // don't process headings
      foreach ($row_cols_value as $col_key => $col_value) {
        if (is_null($col_value)) continue;
        if (strpos($heading[$col_key], 'Metric:') !== FALSE) {
          $col_data_parts = explode(':', $heading[$col_key]);
          $data[$row_key]['Metric'][$col_data_parts[1]][$col_data_parts[2]] = [
            'value' => $col_value,
            'xls_cords' => ['row' => $row_key, 'col' => $col_key],
          ];
        }
        else {
          $data[$row_key][$heading[$col_key]]['value'] = $col_value;
          $data[$row_key][$heading[$col_key]]['xls_cords'] = ['row' => $row_key, 'col' => $col_key];
        }
      }
    }
    return $data;
  }

}