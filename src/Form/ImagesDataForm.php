<?php

namespace Drupal\uani_ibr\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\uani_ibr\Controller\UaniIbr;

class ImagesDataForm extends FormBase {

  public function getFormId() {
    return 'uani_ibr_images_data_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['images_data_files'] = array(
      '#type' => 'managed_file',
      '#multiple' => TRUE,
      '#title' => $this->t('Choose images'),
      '#upload_location' => 'public://ibr/images/',
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg png gif'],
      ],
      '#attached' => [
        'library' => ['file/drupal.file'],
      ],
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save')
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('images_data_files'))) {
      foreach (File::loadMultiple($form_state->getValue('images_data_files')) as $file) {
        $file->setFilename(mb_strtolower(str_replace(' ', '_', $file->getFilename())));
        $file->setPermanent();
        $file->save();
        \Drupal::database()->insert('uani_ibr_uploads')
               ->fields(['type', 'file_id', 'user_id'], ['images', $file->id(), \Drupal::currentUser()->id()])
               ->execute();
      }
      drupal_set_message('Images uploaded!');
      $controller = new UaniIbr();
      $controller->afterSubmitRedirect();
    }
  }


}