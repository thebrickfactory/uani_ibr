<?php

namespace Drupal\uani_ibr\xlsWorker;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\facets\Exception\Exception;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

class graphXls extends xls {

  public function validateXls() {
    \Drupal::state()->set('last_graph_import', $this->file->id());
    if (!$this->canFileBeProcessed()) return false;
    if ($graph_data = $this->getDataArray(true)) {
      // validate headings
      if (count($graph_data[1]) == 4) {
        $heading_validated = TRUE;
        if ($graph_data[1]['A'] != 'Title') {
          $heading_validated = FALSE;
        }
        if ($graph_data[1]['B'] != 'Category') {
          $heading_validated = FALSE;
        }
        if ($graph_data[1]['C'] != 'Iran Number') {
          $heading_validated = FALSE;
        }
        if ($graph_data[1]['D'] != 'Link') {
          $heading_validated = FALSE;
        }
        if ($heading_validated) {
          unset($graph_data[1]);
          // if headings validated - validate content
          // retrieve lightbox data
          $lightbox = new lightboxXls(File::load($this->tempStore->get('lightbox_data_file_id')));
          $content_validated = TRUE;
          // iterate all rows
          foreach ($graph_data as $row_key => $cols) {
            if (count($cols) == 4) {
              // check if title = string
              if (!is_string($cols['A'])) {
                $content_validated = FALSE;
                $this->_validateCellBadValue($row_key, 'A', 'this field must be plain string');
              }
              // check if category exists
              $term_exit_query = \Drupal::entityQuery('taxonomy_term')
                                        ->condition('vid', 'business_risk_categories')
                                        ->condition('name', $cols['B'])
                                        ->count()->execute();
              if ($term_exit_query == 0) {
                $content_validated = FALSE;
                $this->_validateCellBadValue($row_key, 'B', 'category with name "' . $cols['B'] . '" was not found');
              }
              // check if iran number == int
              if (!is_numeric($cols['C'])) {
                $content_validated = FALSE;
                $this->_validateCellBadValue($row_key, 'C', 'this field must be plain string');
              }
              // check if lightbox with specified title exists
              $lightbox_refference_exists = false;
              foreach ($lightbox->getDataArray() as $item) {
                if ($item['Title']['value'] == $cols['D']) $lightbox_refference_exists = true;
              }
              if (!$lightbox_refference_exists) {
                $content_validated = FALSE;
                $this->_validateCellBadValue($row_key, 'D', 'lightbox with title "' . $cols['D'] . '" was not found');
              }
            }
            else {
              $content_validated = FALSE;
              drupal_set_message('Too much columns!', 'error');
            }
          }
          if ($content_validated) {
            $this->xlsStructOk = TRUE;
          }
        }
      }
    }
    return $this->xlsStructOk;
  }

  public function import() {
    if (!$this->xlsStructOk) {
      drupal_set_message('File not validated!', 'error');
      return FALSE;
    }
    if ($graph_data = $this->getDataArray()) {
      $lightbox = new lightboxXls(File::load($this->tempStore->get('lightbox_data_file_id')));
      if ($imported_lightbox_data = $lightbox->import()) {
        $b = 1;
        $this->_deleteOldNodes();
        foreach ($graph_data as $row_key => $cols) {
          $category_query  = \Drupal::entityQuery('taxonomy_term')
                                    ->condition('vid', 'business_risk_categories')
                                    ->condition('name', $cols['B'])
                                    ->execute();
          $category_id     = Term::load(reset($category_query))->id();
          $link_node_query = \Drupal::entityQuery('node')
                                    ->condition('type', 'business_risk_lightbox_data')
                                    ->condition('title', $cols['D'])
                                    ->execute();
          $link_node_id    = Node::load(reset($link_node_query))->id();
          Node::create([
            'type'                            => 'business_risk_graph_data',
            'title'                           => $cols['A'],
            'field_business_risk_category'    => [
              'target_id' => $category_id
            ],
            'field_business_risk_iran_number' => $cols['C'],
            'field_business_risk_link'        => [
              'target_id' => $link_node_id
            ],
          ])->save();
        }
        // unset used lightbox file
        $this->tempStore->set('lightbox_data_uploaded_and_validated', FALSE);
        $this->tempStore->set('lightbox_data_file_id', FALSE);

        return TRUE;
      }
    }
    return FALSE;
  }

  protected function _deleteOldNodes() {
    $graph_data_nodes_query = \Drupal::entityQuery('node')
      ->condition('type', 'business_risk_graph_data')
      ->execute();
    if (!empty($graph_data_nodes_query)) {
      $node_manager     = \Drupal::entityTypeManager()->getStorage('node');
      $graph_data_nodes = Node::loadMultiple($graph_data_nodes_query);
      $node_manager->delete($graph_data_nodes);
    }
  }

  public function getDataArray($with_headings = false) {
    try {
      $sheet = $this->reader->getSheet(0);
    }
    catch(Exception $exception) {
      drupal_set_message($exception->getMessage());
      return false;
    }
    $data = $sheet->toArray(null, true, true, true);
    if (!$with_headings) unset($data[1]); // unset headings
    foreach ($data as $row_key => $cols) {
      // check, maybe this is empty row, so we will miss it
      if (empty($cols['A']) && empty($cols['B']) && empty($cols['C']) && empty($cols['D'])) {
        unset($data[$row_key]);
      }
    }
    return $data;
  }

  public function canFileBeProcessed() {
    if ($this->tempStore->get('lightbox_data_uploaded_and_validated', FALSE) !== TRUE) {
      drupal_set_message(new TranslatableMarkup('Lightbox file not selected/validated!'), 'warning');
      return FALSE;
    }
    return TRUE;
  }

}