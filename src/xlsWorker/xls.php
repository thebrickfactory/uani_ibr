<?php

namespace Drupal\uani_ibr\xlsWorker;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\facets\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class xls {

  public function __construct($file) {
    error_reporting(0);
    $this->file = $file;
    $this->tempStore = new Session();
    $this->pe = get_php_excel();
    $this->xlsStructOk = false;
    $this->_loadReader();
  }

  public function validateXls() {}

  protected function _loadReader() {
    $file_uri = $this->file->getFileUri();
    try {
      $this->reader = \PHPExcel_IOFactory::load($file_uri);
    }
    catch (Exception $exception) {
      drupal_set_message($exception->getMessage());
    }
  }

  protected function _validateCellBadValue($row = '', $col = '', $custom_text = '') {
    $errormsg = new TranslatableMarkup('Cell :col:row has incorrect value, please fix and try again. ', array(':col' => $col, ':row' => $row));
    if ($custom_text != '') $errormsg .= new TranslatableMarkup('Details: ').$custom_text;
    drupal_set_message($errormsg, 'error');
  }

  public function import() {}

  protected function _deleteOldNodes() {}

  public function getDataArray() {}

}